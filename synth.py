import numpy as np
from scipy.signal import square, sawtooth

# TODO:
# Composition class: write melodies, harmonies and rythm with constant bpm

def supported_instruments():
    """Return a list of supported instruments"""
    return ['sine',
            'square',
            'sawtooth',
            'pulse10',
            'pulse30']


def supported_tunings():
    """Return a list of supported tunings"""
    return ['equal',
            'just']


class Channel:
    def __init__(self, synth, start=0, instrument='sine'):
        self.instrument = instrument
        self.start = start
        self.track = np.empty(0)
        self.synth = synth

    def __iter__(self):
        yield self

    def add(self, chord, length, scale=.8):
        """Add new wave to current wave
        :chord:
        :length:
        :instrument:
        :scale: scaling factor (default 0.8)
        :returns: complete wave
        """
        wave = synth.gen_wave(chord, length, self.instrument)
        wave = wave / wave.max() * scale
        self.track = np.append(self.track, wave)

        return wave

    def info(self):
        """Print information about channel"""
        print("Instrument: " + self.instrument)
        print("Start: %d seconds" % self.start)
        print("Length: %.2f seconds" % (len(self.track)/synth.sampling_freq))


class Synth:
    def __init__(self, A4=440, tuning='equal', sampling_freq=44100):
        # Dictionary of notes
        self.dict_notes = {'A': 0, 'A#': 1, 'Bb': 1, 'B': 2,
                           'C': 3, 'C#': 4, 'Db': 4, 'D': 5,
                           'D#': 6, 'Eb': 6, 'E': 7, 'F': 8,
                           'F#': 9, 'Gb': 9, 'G': 10, 'G#': 11,
                           'Ab': 11}
        self.A4 = A4
        self.tuning = tuning
        self.generate_tuning(A4, tuning)
        self.sampling_freq = sampling_freq
        self.channels = []

    def add_channel(self, instrument='sine', start=0):
        """Add new channel
        self.tuning = tuning
        :instrument: default instrument
        :start: starting second
        :returns: index of channel
        """
        ch = Channel(self, start, instrument=instrument)
        self.channels.append(ch)
        return len(self.channels) - 1

    def info(self):
        """Print info on synth and channels"""
        print("Synth information")
        print("---")
        print("A4: %d" % self.A4)
        print("Tuning: " + self.tuning)
        print("Sampling frequency: %d" % self.sampling_freq)
        print()
        for ix, ch in enumerate(self.channels):
            print("Channel #%d:" % ix)
            ch.info()

    def generate_tuning(self, A4=440, tuning='equal'):
        """Generate vector of frequencies for given tuning
        :A4: A4 frequency
        :tuning: Check `supported_tunings()` for valid tunings
        """
        if tuning not in supported_tunings():
            raise ValueError(tuning + ' is not supported. Check '
                             '`supported_tunings()`')

        if tuning == 'equal':
            # Compute ratios starting from A4
            ratios = 2**(np.arange(12)/12)
        elif tuning == 'just':
            ratios = np.array([1, 25/24, 9/8, 6/5, 5/4, 4/3,
                               45/32, 3/2, 8/5, 5/3, 9/5, 15/8])
        self.freqs = A4*ratios

    def convert_note_to_frequency(self, notes):
        """Convert note string (english notation + octave e.g. E4)
        :notes: note string or list of notes
        :return: frequency value
        """
        # Parse note
        if type(notes) is not list:
            notes = [notes]

        notes_freqs = np.zeros(len(notes))
        for i, n in enumerate(notes):
            key = n[:-1]
            octave = n[-1]

            if key not in self.dict_notes.keys():
                raise ValueError(
                    n + ' is invalid. Format: <key><octave> where '
                    'octave is 0-9')

            try:
                octave = int(octave)
            except ValueError:
                raise ValueError(
                    'Octave must be a number greater or equal than 0.')
            if octave < 0:
                raise ValueError(
                    'Octave must be a number greater or equal than 0.')

            notes_freqs[i] = self.freqs[self.dict_notes[key]]*2**(octave - 4)

        if len(notes_freqs) == 1:
            return notes_freqs[0]
        else:
            return notes_freqs

    def gen_wave(self, chord, length, instrument):
        """Generate a waveform using the given chord (list) or note (string)

        :chord: a list of strings (or string) with the notes of a chord or
        notes
        :length: length of the waveform in seconds
        :instrument: type of instrument. Check `supported_instruments()`
        function
        :returns wave: numpy array
        """

        if type(chord) is not list:
            chord = [chord]

        try:
            length = int(length)
        except ValueError:
            raise ValueError('Length is not a number')

        if length <= 0:
            raise ValueError('Length must be greater than 0')

        samples = int(length * self.sampling_freq)
        t = np.arange(samples)/self.sampling_freq
        f = self.convert_note_to_frequency(chord)

        if type(f) is np.ndarray:
            arg = 2 * np.pi * f[:, np.newaxis] * t
        else:
            arg = 2 * np.pi * f * t

        if instrument == 'sine':
            wave = np.sin(arg)
        elif instrument == 'square':
            wave = square(arg)
        elif instrument == 'sawtooth':
            wave = sawtooth(arg)
        elif instrument == 'pulse10':
            wave = square(arg, duty=.1)
        elif instrument == 'pulse30':
            wave = square(arg, duty=.3)
        elif callable(instrument):  # custom instrument
            wave = instrument(arg)

        if len(wave.shape) == 2:
            wave = np.sum(wave, axis=0)

        return wave

    def mix_channels(self):
        max_length = 0
        n = len(self.channels)
        for ch in self.channels:
            end = ch.start + len(ch.track)
            if max_length < end:
                max_length = end
        tracks = np.zeros((n, max_length))
        for ix, ch in enumerate(self.channels):
            tracks[ix, ch.start:len(ch.track)] = ch.track.copy()

        return np.mean(tracks, axis=0)


if __name__ == '__main__':
    synth = Synth()
    synth.add_channel('square')
    synth.add_channel('sine')
    synth.add_channel('sine')
    synth.channels[0].add(['E3', 'G#3'], 2)
    synth.channels[1].add('G3', 2)
    synth.channels[2].add('A#3', 2)

    import soundcard as sc
    ds = sc.default_speaker()
    ds.play(synth.mix_channels(), samplerate=synth.sampling_freq)

